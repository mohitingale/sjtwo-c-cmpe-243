#include "switch_logic.h"
#include "board_io.h"

gpio_s switch_logic__initialize() {
  gpio_s input_sw;
  input_sw = gpio__construct_as_input(GPIO__PORT_0, 30);
  gpio__set_as_input(input_sw);
  return input_sw;
}

void my_valid() {}

void switch_logic__run_once(gpio_s input_sw) {
  if (!gpio__get(input_sw)) {
    gpio__set(board_io__get_led1());
  } else {
    gpio__reset(board_io__get_led1());
  }
}