#include "can_dbc_handler.h"
#include "board_io.h"
#include "can_bus.h"
#include "gpio.h"
#include "lab_5.h"
#include <stdint.h>
#include <stdio.h>

gpio_s mia_gps_fault_led;

static dbc_GPS_DATA_s decoded_gps_data = {};

void can_dbc_handler__init(void) {
  // mia_gps_fault_led = gpio__construct_with_function(GPIO__PORT_2, 3, GPIO__FUNCITON_0_IO_PIN);
  can__init(can1, 100, 50, 50, NULL, NULL);
  can__bypass_filter_accept_all_msgs();
  can__reset_bus(can1);
}

void can_dbc_handler__reset_if_bus_off(void) {
  if (can__is_bus_off(can1)) {
    can__reset_bus(can1);
  }
}

bool can_dbc_handler__gps_message_send(void) {
  dbc_GPS_DATA_s gps_data;
  gps_coordinates_t gps_coordinates = gps__get_coordinates();
  gps_data.GPS_DATA_latitude = gps_coordinates.latitude;
  gps_data.GPS_DATA_longitude = gps_coordinates.longitude;
  can__msg_t can_msg = {};
  const dbc_message_header_t header = dbc_encode_GPS_DATA(can_msg.data.bytes, &gps_data);
  can_msg.msg_id = header.message_id;
  can_msg.frame_fields.data_len = header.message_dlc;
  return can__tx(can1, &can_msg, 0);
}

bool can_dbc_handler__test_message_send(void) {
  dbc_TEST_DATA_s test_data;
  test_data.TEST_DATA_signed = -5.5;
  test_data.TEST_DATA_unsigned = 4.4;
  can__msg_t can_msg = {};
  const dbc_message_header_t header = dbc_encode_TEST_DATA(can_msg.data.bytes, &test_data);
  can_msg.msg_id = header.message_id;
  can_msg.frame_fields.data_len = header.message_dlc;
  return can__tx(can1, &can_msg, 0);
}

bool can_dbc_handler__gps_message_receive(gps_coordinates_t *gps_data) {
  bool return_flag = false;
  can__msg_t can_msg = {};
  //   gps_coordinates_t gps_coordinates = {};
  while (can__rx(can1, &can_msg, 0)) {
    const dbc_message_header_t header = {.message_id = can_msg.msg_id, .message_dlc = can_msg.frame_fields.data_len};

    if (dbc_decode_GPS_DATA(&decoded_gps_data, header, can_msg.data.bytes)) {
      gps_data->latitude = decoded_gps_data.GPS_DATA_latitude;
      gps_data->longitude = decoded_gps_data.GPS_DATA_longitude;
      printf("can:Latitude = %lf, Longitude= %lf\n", (double)gps_data->latitude, (double)gps_data->longitude);
      gpio__set(mia_gps_fault_led);
      return_flag = true;
    }
  }
  return return_flag;
}

void can_dbc_handler__test_message_receive(void) {
  can__msg_t can_msg = {};
  //   gps_coordinates_t gps_coordinates = {};
  dbc_TEST_DATA_s decoded_test_data;
  while (can__rx(can1, &can_msg, 0)) {
    const dbc_message_header_t header = {.message_id = can_msg.msg_id, .message_dlc = can_msg.frame_fields.data_len};

    // Even if incoming message is NOT motor cmd, our decode functions
    // will gracefully handle it because we provide valid "message header"
    if (dbc_decode_TEST_DATA(&decoded_test_data, header, can_msg.data.bytes)) {
      printf("Signed = %lf Unsigned = %lf\n", (double)decoded_test_data.TEST_DATA_signed,
             (double)decoded_test_data.TEST_DATA_unsigned);
    }
  }
}

void can_handler__manage_mia_10hz(void) {
  // We are in 10hz slot, so increment MIA counter by 100ms
  // gpio__set(mia_gps_fault_led);
  const uint32_t mia_increment_value = 100;
  if (dbc_service_mia_GPS_DATA(&decoded_gps_data, mia_increment_value)) {
    gpio__reset(board_io__get_led3());
  } else {
  }
}