

#include "FreeRTOS.h"

#include <stdlib.h>

#include "uart.h"
#include "uart_printf.h"

#include "clock.h" // needed for UART initialization
#include "gpio.h"
#include "queue.h"

#include "fake_gps.h" // TODO: You need to create this module, unit-tests for this are optional
#include <stdio.h>
// Change this according to which UART you plan to use
static uart_e gps_uart = UART__3;

void fake_gps__init(void) {
  gpio__construct_with_function(GPIO__PORT_4, 28, GPIO__FUNCTION_2);
  gpio__construct_with_function(GPIO__PORT_4, 29, GPIO__FUNCTION_2);
  uart__init(gps_uart, clock__get_peripheral_clock_hz(), 38400);

  QueueHandle_t rxq_handle = xQueueCreate(4, sizeof(char));   // Nothing to receive
  QueueHandle_t txq_handle = xQueueCreate(100, sizeof(char)); // We send a lot of data
  uart__enable_queues(gps_uart, rxq_handle, txq_handle);
}

static float generate_random_float() {
  float a = 90.0;
  float x = (float)rand() / (float)(RAND_MAX / a);
  return x;
}

/// Send a fake GPS string
/// TODO: You may want to be somewhat random about the coordinates that you send here
void fake_gps__run_once(void) {
  float latitude = generate_random_float();
  float longitude = generate_random_float();
  uart_printf(gps_uart, "$GPGGA,230612.015,%4.4f,S,%4.4f,W,0,04,5.7,508.3,M,,,,0000*13\r\n", (double)latitude,
              (double)longitude);
  // printf("$GPGGA,230612.015,%4.4f,S,%4.4f,W,0,04,5.7,508.3,M,,,,0000*13\r\n", (double)latitude, (double)longitude);
}