#include "unity.h"

#include <stdio.h>

#include "Mockboard_io.h"
#include "Mockcan_bus.h"
#include "Mockgpio.h"
#include "Mockgps.h"

#include "can_dbc_handler.c"
#include "can_dbc_mia_config.c"
#include "lab_5.h"

void setUp() {}

void tearDown() {}

void test_can_dbc_handler__init(void) {
  can__init_ExpectAndReturn(can1, 100, 50, 50, NULL, NULL, true);
  can__bypass_filter_accept_all_msgs_Expect();
  can__reset_bus_Expect(can1);
  can_dbc_handler__init();
}

void test_can_dbc_handler__reset_if_bus_off(void) {
  can__is_bus_off_ExpectAndReturn(can1, true);
  can__reset_bus_Expect(can1);
  can_dbc_handler__reset_if_bus_off();
}

void test_can_dbc_handler__dbc_encode_decode_GPS_DATA_value_in_range(void) {
  dbc_GPS_DATA_s gps_data;
  dbc_GPS_DATA_s received_gps_data;
  gps_data.GPS_DATA_latitude = 11.4;
  gps_data.GPS_DATA_longitude = 19.4;
  can__msg_t can_msg = {};
  const dbc_message_header_t header = dbc_encode_GPS_DATA(can_msg.data.bytes, &gps_data);
  TEST_ASSERT_EQUAL_UINT32(100, header.message_id);
  TEST_ASSERT_EQUAL_UINT32(8, header.message_dlc);
  dbc_decode_GPS_DATA(&received_gps_data, header, can_msg.data.bytes);
  TEST_ASSERT_EQUAL_FLOAT(received_gps_data.GPS_DATA_latitude, gps_data.GPS_DATA_latitude);
  TEST_ASSERT_EQUAL_FLOAT(received_gps_data.GPS_DATA_longitude, gps_data.GPS_DATA_longitude);
}

void test_can_dbc_handler__dbc_encode_decode_GPS_DATA_exceed_max_value(void) {
  dbc_GPS_DATA_s gps_data;
  dbc_GPS_DATA_s received_gps_data;
  gps_data.GPS_DATA_latitude = 101.46767674343;
  gps_data.GPS_DATA_longitude = 190.4982737467983475;
  can__msg_t can_msg = {};
  const dbc_message_header_t header = dbc_encode_GPS_DATA(can_msg.data.bytes, &gps_data);
  TEST_ASSERT_EQUAL_UINT32(100, header.message_id);
  TEST_ASSERT_EQUAL_UINT32(8, header.message_dlc);
  dbc_decode_GPS_DATA(&received_gps_data, header, can_msg.data.bytes);
  TEST_ASSERT_EQUAL_FLOAT(received_gps_data.GPS_DATA_latitude, 91.1111111);
  TEST_ASSERT_EQUAL_FLOAT(received_gps_data.GPS_DATA_longitude, 181.1111111);
}

void test_can_dbc_handler__gps_message_send_negative_value(void) {
  dbc_GPS_DATA_s gps_data;
  dbc_GPS_DATA_s received_gps_data;
  gps_data.GPS_DATA_latitude = -11.4;
  gps_data.GPS_DATA_longitude = -29.4;
  can__msg_t can_msg = {};
  const dbc_message_header_t header = dbc_encode_GPS_DATA(can_msg.data.bytes, &gps_data);
  TEST_ASSERT_EQUAL_UINT32(100, header.message_id);
  TEST_ASSERT_EQUAL_UINT32(8, header.message_dlc);
  dbc_decode_GPS_DATA(&received_gps_data, header, can_msg.data.bytes);
  TEST_ASSERT_EQUAL_FLOAT(received_gps_data.GPS_DATA_latitude, gps_data.GPS_DATA_latitude);
  TEST_ASSERT_EQUAL_FLOAT(received_gps_data.GPS_DATA_longitude, gps_data.GPS_DATA_longitude);
}

// void test_can_dbc_handler__dbc_encode_decode_GPS_DATA_exceed_min_value(void) {
//   dbc_GPS_DATA_s gps_data;
//   dbc_GPS_DATA_s received_gps_data;
//   gps_data.GPS_DATA_latitude = -101.46767674343;
//   gps_data.GPS_DATA_longitude = -190.4982737467983475;
//   can__msg_t can_msg = {};
//   const dbc_message_header_t header = dbc_encode_GPS_DATA(can_msg.data.bytes, &gps_data);
//   TEST_ASSERT_EQUAL_UINT32(100, header.message_id);
//   TEST_ASSERT_EQUAL_UINT32(8, header.message_dlc);
//   dbc_decode_GPS_DATA(&received_gps_data, header, can_msg.data.bytes);
//   TEST_ASSERT_EQUAL_FLOAT(received_gps_data.GPS_DATA_latitude, -91.1111111);
//   TEST_ASSERT_EQUAL_FLOAT(received_gps_data.GPS_DATA_longitude, -181.1111111);
// }

void test_can_handler__manage_mia_10hz(void) {
  gpio_s board_led_3 = {GPIO__PORT_2, 3};
  const uint32_t mia_increment_value = 100;
  can_handler__manage_mia_10hz();
  board_io__get_led3_ExpectAndReturn(board_led_3);
  gpio__reset_Expect(board_led_3);
  decoded_gps_data.mia_info.mia_counter = dbc_mia_threshold_GPS_DATA - mia_increment_value;
  can_handler__manage_mia_10hz();
}