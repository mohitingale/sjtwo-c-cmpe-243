#include "FreeRTOS.h"

#include <stdio.h>

#include "board_io.h"
#include "gpio.h"
#include "periodic_callbacks.h"

#include "switch_logic.h"
#include "task.h"

#include "fake_gps.h"
#include "gps.h"

#include "can_dbc_handler.h"
#include "can_message_sensor_handle.h"

static gpio_s input_gpio;
can__num_e canbus = can1;

/******************************************************************************
 * Your board will reset if the periodic function does not return within its deadline
 * For 1Hz, the function must return within 1000ms
 * For 1000Hz, the function must return within 1ms
 */

void periodic_callbacks__initialize(void) {
  input_gpio = switch_logic__initialize();
#if (FAKE_GPS)
  fake_gps__init();
#else
  gps__init();
#endif
  can_dbc_handler__init();
}

void periodic_callbacks__1Hz(uint32_t callback_count) {
// gpio__toggle(board_io__get_led0());
#if (FAKE_GPS)
  fake_gps__run_once();
#else
  gps__run_once();
  gps_coordinates_t temp_coordinates;
  temp_coordinates = gps__get_coordinates();
  printf("Latitude = %f and Longitude = %f\n", (double)temp_coordinates.latitude, (double)temp_coordinates.longitude);
#endif
  can_dbc_handler__reset_if_bus_off();
}

void periodic_callbacks__10Hz(uint32_t callback_count) {
#if (FAKE_GPS)
  gps_coordinates_t gps_data = {};
  can_dbc_handler__gps_message_receive(&gps_data);
  can_dbc_handler__test_message_send();
  can_handler__manage_mia_10hz();
#else
  can_dbc_handler__gps_message_send();
  can_dbc_handler__test_message_receive();
#endif
}

void periodic_callbacks__100Hz(uint32_t callback_count) {
  // #if (!FAKE_GPS)
  //   can_dbc_handler__test_message_receive();
  // #else
  //   can_dbc_handler__test_message_send();
  // #endif
}

/**
 * @warning
 * This is a very fast 1ms task and care must be taken to use this
 * This may be disabled based on intialization of periodic_scheduler__initialize()
 */
void periodic_callbacks__1000Hz(uint32_t callback_count) {
  // gpio__toggle(board_io__get_led3());
  // Add your code here
}