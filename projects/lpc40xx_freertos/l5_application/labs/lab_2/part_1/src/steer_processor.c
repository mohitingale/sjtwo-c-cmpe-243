#include "steer_processor.h"

void steer_processor(uint32_t left_sensor_cm, uint32_t right_sensor_cm) {
  static int threshold = 50;
  if (left_sensor_cm < threshold && right_sensor_cm < threshold) {
    if (left_sensor_cm > right_sensor_cm) {
      steer_left();
    } else if (left_sensor_cm < right_sensor_cm) {
      steer_right();
    }
  }
  return;
}