#include "can_bus_handle.h"
#include <stdio.h>
#include <string.h>

#include "gpio.h"

void can_bus_handle_init(can__num_e canbus) {
  can__init(canbus, 100, 50, 50, NULL, NULL);
  can__bypass_filter_accept_all_msgs();
  can__reset_bus(canbus);
}

void can_bus_handle__reset_if_bus_off(can__num_e canbus) {
  if (can__is_bus_off(canbus)) {
    can__reset_bus(canbus);
  }
}

bool can_bus_handle_send_byte(can__num_e canbus, uint8_t message_id, uint8_t message, uint32_t timeout) {
  can__msg_t data = {0};
  data.msg_id = message_id;
  data.frame_fields.data_len = 1;
  data.data.bytes[0] = message;
  return can__tx(canbus, &data, timeout);
}

bool can_bus_handle_receive_byte(can__num_e canbus, uint8_t message_id, uint8_t *message, uint32_t timeout) {
  bool return_val = false;
  can__msg_t rx_data = {0};
  rx_data.msg_id = message_id;
  rx_data.frame_fields.data_len = 1;
  if (can__rx(canbus, &rx_data, 0)) {
    return_val = true;
    *message = rx_data.data.bytes[0];
  }
  return return_val;
}

bool can_bus_handle__send_message(can__num_e canbus, can_message_handle_t can_message) {
  can__msg_t tx_data = {0};
  tx_data.msg_id = can_message.message_id;
  tx_data.frame_fields.data_len = can_message.data_length;
  memcpy(&tx_data.data.qword, &can_message.data, sizeof(tx_data.data.qword));
  return can__tx(canbus, &tx_data, can_message.timeout);
}

bool can_bus_handle__receive_message(can__num_e canbus, can_message_handle_t *can_message) {
  bool return_val = false;
  can__msg_t rx_data = {0};
  if (can__rx(canbus, &rx_data, can_message->timeout)) {
    return_val = true;
    memcpy(&can_message->data, &rx_data.data.qword, sizeof(can_message->data));
    can_message->message_id = rx_data.msg_id;
    can_message->data_length = rx_data.frame_fields.data_len;
  }
  return return_val;
}
