#pragma once

#define FAKE_GPS 1
void fake_gps__init(void);

/// Send a fake GPS string
/// TODO: You may want to be somewhat random about the coordinates that you send here
void fake_gps__run_once(void);