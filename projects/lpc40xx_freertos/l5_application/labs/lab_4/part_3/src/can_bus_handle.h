#pragma once

#include "can_bus.h"

typedef struct {
  uint32_t data_length;
  uint32_t message_id;
  uint32_t timeout;
  uint64_t data;
} can_message_handle_t;

void can_bus_handle_init(can__num_e canbus);
void can_bus_handle__reset_if_bus_off(can__num_e canbus);
bool can_bus_handle_send_byte(can__num_e canbus, uint8_t message_id, uint8_t message, uint32_t timeout);

bool can_bus_handle_receive_byte(can__num_e canbus, uint8_t message_id, uint8_t *message, uint32_t timeout);

bool can_bus_handle__send_message(can__num_e canbus, can_message_handle_t can_message);
bool can_bus_handle__receive_message(can__num_e canbus, can_message_handle_t *can_message);
