#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

/* In this code lab, the queue memory is statically defined
 * and fixed at compile time for 100 uint8s
 */
typedef struct {
  uint8_t *queue_memory;
  uint8_t queue_top;
  uint8_t queue_size;

  // TODO: Add more members as needed
} queue_s;

void queue_static__init(queue_s *queue, void *static_queue_memory, size_t size_of_static_memory);

/// @returns false if the queue is full
bool queue_static__push(queue_s *queue, uint8_t push_value);

/// @returns false if the queue was empty
bool queue_static__pop(queue_s *queue, uint8_t *pop_value);

size_t queue_static__get_item_count(const queue_s *queue);
