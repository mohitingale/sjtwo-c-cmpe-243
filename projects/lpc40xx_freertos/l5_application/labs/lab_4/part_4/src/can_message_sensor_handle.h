#pragma once

#include "can_bus_handle.h"
#include "gps.h"

bool can_sensor_send_gps_data(can__num_e canbus);
bool can_sensor_receive_gps_data(can__num_e canbus, gps_coordinates_t *gps_data);