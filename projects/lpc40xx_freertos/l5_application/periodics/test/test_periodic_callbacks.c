#include <stdio.h>
#include <string.h>

#include "unity.h"

// Include the Mocks
// - This will not pull the REAL source code of these modules (such as board_io.c)
// - This will auto-generate "Mock" versions based on the header file
#include "Mockboard_io.h"
#include "Mockcan_dbc_handler.h"
#include "Mockcan_message_sensor_handle.h"
#include "Mockfake_gps.h"
#include "Mockgpio.h"
#include "Mockgps.h"
#include "Mockswitch_logic.h"

// Include the source we wish to test
#include "periodic_callbacks.h"
static gpio_s input_gpio;

can__num_e canbus1 = can1;

void setUp(void) {}

void tearDown(void) {}

void test__periodic_callbacks__initialize(void) {
  if (FAKE_GPS) {
    fake_gps__init_Expect();
  } else {
    gps__init_Expect();
  }
  switch_logic__initialize_ExpectAndReturn(input_gpio);
  can_dbc_handler__init_Expect();
  periodic_callbacks__initialize();
}

void test__periodic_callbacks__1Hz(void) {
  if (FAKE_GPS) {
    fake_gps__run_once_Expect();
  } else {
    gps_coordinates_t temp_coordinates;
    gps__run_once_Ignore();
    gps__get_coordinates_ExpectAndReturn(temp_coordinates);
  }
  can_dbc_handler__reset_if_bus_off_Expect();
  periodic_callbacks__1Hz(0);
}

// void test__periodic_callbacks__10Hz(void) {
//   uint8_t message_id = 1;
//   uint8_t message = 'c';
//   can_bus_handle_send_byte_ExpectAndReturn(canbus1, message_id, message, 0, true);
//   message_id = 2;
//   can_bus_handle_receive_byte_ExpectAndReturn(canbus1, message_id, &message, 0, false);
//   periodic_callbacks__10Hz(0);
// }

// void test__periodic_callbacks__10Hz(void) {
//   can__num_e canbus = can1;
//   gps_coordinates_t data = {};
//   if (FAKE_GPS) {
//     can_sensor_receive_gps_data_ExpectAndReturn(canbus1, &data, true);
//   } else {
//     can_sensor_send_gps_data_ExpectAndReturn(canbus1, true);
//   }
//   periodic_callbacks__10Hz(0);
// }

void test__periodic_callbacks__10Hz(void) {
  gps_coordinates_t gps_data = {};
  if (FAKE_GPS) {
    can_dbc_handler__gps_message_receive_ExpectAndReturn(&gps_data, true);
    can_dbc_handler__test_message_send_ExpectAndReturn(true);
    can_handler__manage_mia_10hz_Expect();
  } else {
    can_dbc_handler__gps_message_send_ExpectAndReturn(true);
    can_dbc_handler__test_message_receive_Expect();
  }
  periodic_callbacks__10Hz(0);
}

// void test__periodic_callbacks__100Hz(void) {
//   if (!FAKE_GPS) {
//     can_dbc_handler__test_message_receive_Expect();
//   } else {
//     can_dbc_handler__test_message_send_ExpectAndReturn(true);
//   }
//   periodic_callbacks__100Hz(0);
// }
