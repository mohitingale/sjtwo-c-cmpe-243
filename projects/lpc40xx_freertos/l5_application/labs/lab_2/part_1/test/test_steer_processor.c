#include "unity.h"

#include "steer_processor.h"

#include "Mocksteering.h"

void setUp(void) {}

void tearDown(void) {}

void test_steer_processor__move_left(void) {
  steer_left_Expect();
  steer_processor(40, 10);
}

void test_steer_processor__move_right(void) {
  steer_right_Expect();
  steer_processor(10, 40);
}

void test_steer_processor__both_sensor_above_threshold(void) {
  steer_right_Ignore();
  steer_left_Ignore();
  steer_processor(50, 50);
}

void test_steer_processor__both_sensor_below_threshold(void) {
  steer_right_Expect();
  steer_processor(10, 20);
  steer_left_Expect();
  steer_processor(20, 10);
}

void test_steer_processor(void) {
  steer_right_Expect();
  steer_processor(10, 20);
  steer_left_Expect();
  steer_processor(20, 10);
}