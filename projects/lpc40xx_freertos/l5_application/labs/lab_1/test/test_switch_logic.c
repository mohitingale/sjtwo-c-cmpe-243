#include <stdio.h>

#include "unity.h"

#include "Mockboard_io.h"
#include "Mockgpio.h"

#include "switch_logic.h"

void setUp(void) {}

void test__switch_logic__initialize() {
  //   my_valid();
  gpio_s gpio = {GPIO__PORT_0, 30};
  gpio__construct_as_input_ExpectAndReturn(GPIO__PORT_0, 30, gpio);
  gpio__set_as_input_Expect(gpio);
  gpio = switch_logic__initialize();
  gpio_s test_gpio = {GPIO__PORT_0, 30};
}

void test__switch_logic__run_once__set() {
  gpio_s input_gpio = {GPIO__PORT_0, 30};
  gpio_s output_gpio = {};

  bool val = true;
  gpio__get_ExpectAndReturn(input_gpio, val);
  board_io__get_led1_ExpectAndReturn(output_gpio);
  gpio__reset_Expect(output_gpio);
  switch_logic__run_once(input_gpio);
}

void test__switch_logic__run_once___reset() {
  gpio_s input_gpio = {GPIO__PORT_0, 30};
  gpio_s output_gpio = {};

  bool val = false;
  gpio__get_ExpectAndReturn(input_gpio, val);
  board_io__get_led1_ExpectAndReturn(output_gpio);
  gpio__set_Expect(output_gpio);
  switch_logic__run_once(input_gpio);
}