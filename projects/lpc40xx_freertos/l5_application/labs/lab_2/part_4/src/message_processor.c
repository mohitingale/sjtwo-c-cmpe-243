#include <stddef.h>
#include <string.h>

#include "message_processor.h"

#include "message.h"

static bool message_processor_check_dollar_in_message(const message_s *message) {
  if (message->data[0] == '$') {
    return true;
  } else {
    return false;
  }
}

bool message_processor(void) {
  bool symbol_found = false;
  message_s message;
  memset(&message, 0, sizeof(message));

  const static size_t max_messages_to_process = 3;
  for (size_t message_count = 0; message_count < max_messages_to_process; message_count++) {
    if (!message__read(&message)) {
      break;
    } else {
      symbol_found = message_processor_check_dollar_in_message(&message);
    }
  }
  return symbol_found;
}