#include "unity.h"

#include "Mockcan_bus.h"
#include "Mockcan_bus_handle.h"
#include "Mockgps.h"
#include "can_message_sensor_handle.h"

#include "string.h"

void setUp(void) {}

void tearDown(void) {}

bool can_receive_gps_data(can__num_e canbus, can_message_handle_t *can_message, int callback_count) {
  gps_coordinates_t gps_data;
  gps_data.latitude = 1.543f;
  gps_data.longitude = 234.778f;
  memcpy(&can_message->data, &gps_data, sizeof(gps_data));
  if (callback_count == 0) {
    return true;
  } else {
    return false;
  }
}

void test_can_sensor_send_gps_data(void) {
  can__num_e canbus;
  gps_coordinates_t gps_data = {0};
  gps_data.latitude = 1.543f;
  gps_data.longitude = 234.778f;
  gps__get_coordinates_ExpectAndReturn(gps_data);
  can_bus_handle__send_message_IgnoreAndReturn(true);
  TEST_ASSERT_TRUE(can_sensor_send_gps_data(canbus));
}

void test_can_sensor_receive_gps_data(void) {
  can_bus_handle__receive_message_StubWithCallback(can_receive_gps_data);
  can__num_e canbus;
  gps_coordinates_t gps_data = {};
  TEST_ASSERT_TRUE(can_sensor_receive_gps_data(canbus, &gps_data));

  TEST_ASSERT_EQUAL_FLOAT(1.543f, gps_data.latitude);
  TEST_ASSERT_EQUAL_FLOAT(234.778f, gps_data.longitude);
}
