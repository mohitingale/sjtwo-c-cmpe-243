#include "unity.h"

#include "Mockclock.h"
#include "Mockgpio.h"
#include "Mockqueue.h"
#include "Mockuart.h"
#include "Mockuart_printf.h"

#include "fake_gps.c"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static const uart_e test_gps_uart = UART__3;
static const size_t test_clock = 96 * 1000 * 1000;
static const size_t test_baudrate = 38400;

void test_fake_gps__init(void) {
  gpio_s rx_pin, tx_pin;
  QueueHandle_t test_rx_buffer, test_tx_buffer = NULL;
  clock__get_peripheral_clock_hz_ExpectAndReturn(test_clock);
  uart__init_Expect(test_gps_uart, test_clock, test_baudrate);

  gpio__construct_with_function_ExpectAndReturn(4, 28, GPIO__FUNCTION_2, rx_pin);
  gpio__construct_with_function_ExpectAndReturn(4, 29, GPIO__FUNCTION_2, tx_pin);

  xQueueCreate_ExpectAndReturn(4, sizeof(char), test_rx_buffer);
  xQueueCreate_ExpectAndReturn(100, sizeof(char), test_tx_buffer);
  uart__enable_queues_ExpectAndReturn(test_gps_uart, test_rx_buffer, test_tx_buffer, true);
  fake_gps__init();
}

void test_fake_gps__run_once(void) {
  uart_printf_ExpectAndReturn(gps_uart, NULL, 0);
  uart_printf_IgnoreArg_format();
  fake_gps__run_once();
}