#include "lab_queue.h"

void queue__init(queue_s *queue) {
  for (uint8_t i = 0; i < QUEUE_SIZE; i++) {
    queue->queue_memory[i] = 0;
  }
  queue->queue_top = 0;
}

static void queue__shift_elements(queue_s *queue) {
  for (uint8_t i = 0; i < (queue->queue_top - 1); i++) {
    queue->queue_memory[i] = queue->queue_memory[i + 1];
  }
  (queue->queue_top)--;
}

bool queue__push(queue_s *queue, uint8_t push_value) {
  if (queue__get_item_count(queue) < QUEUE_SIZE) {
    queue->queue_memory[(queue->queue_top)++] = push_value;
    return true;
  } else {
    return false;
  }
}

bool queue__pop(queue_s *queue, uint8_t *pop_value) {
  if (queue__get_item_count(queue) > 0) {

    *pop_value = queue->queue_memory[0];
    queue__shift_elements(queue);
    return true;
  } else {
    return false;
  }
}

size_t queue__get_item_count(const queue_s *queue) { return queue->queue_top; }