#pragma once
#include "gps.h"
#include "stdbool.h"

void can_dbc_handler__init(void);
void can_dbc_handler__reset_if_bus_off(void);
bool can_dbc_handler__gps_message_receive(gps_coordinates_t *gps_data);
bool can_dbc_handler__gps_message_send(void);

bool can_dbc_handler__test_message_send(void);
void can_dbc_handler__test_message_receive(void);

void can_handler__manage_mia_10hz(void);