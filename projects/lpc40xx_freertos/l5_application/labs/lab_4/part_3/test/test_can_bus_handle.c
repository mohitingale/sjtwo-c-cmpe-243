#include "unity.h"

#include "Mockcan_bus.h"
#include "Mockgpio.h"
#include "can_bus_handle.h"

can__num_e canbus = can1;

void setUp(void) {}

void tearDown(void) {}

void test_can_bus_handle_init(void) {
  // gpio_s mia_gps_fault_led = {2, 3};
  can__init_ExpectAndReturn(canbus, 100, 50, 50, NULL, NULL, true);
  can__bypass_filter_accept_all_msgs_Expect();
  can__reset_bus_Expect(canbus);
  // gpio__construct_with_function_ExpectAndReturn(GPIO__PORT_2, 3, GPIO__FUNCITON_0_IO_PIN, mia_gps_fault_led);
  // gpio__set_Expect(mia_gps_fault_led);
  can_bus_handle_init(canbus);
}

void test_can_bus_handle_send_byte(void) {
  can__msg_t data = {0};
  data.msg_id = 1;
  data.frame_fields.data_len = 1;
  data.data.bytes[0] = 'c';
  uint32_t timeout = 0;
  can__tx_ExpectAndReturn(canbus, &data, timeout, true);
  can_bus_handle_send_byte(canbus, 1, 'c', 0);
}

void test_can_bus_handle_receive_byte(void) {
  can__msg_t rx_data = {0};
  rx_data.msg_id = 2;
  rx_data.frame_fields.data_len = 1;
  can__rx_ExpectAndReturn(canbus, &rx_data, 0, false);
  uint8_t message;
  can_bus_handle_receive_byte(canbus, 2, &message, 0);
}

void test_can_bus_handle__send_message(void) {
  can_message_handle_t can_message = {0};
  can__msg_t tx_data = {0};
  can__tx_ExpectAndReturn(canbus, &tx_data, can_message.timeout, true);
  can_bus_handle__send_message(canbus, can_message);
}

void test_can_bus_handle_receive_message(void) {
  can__msg_t rx_data = {0};
  can_message_handle_t can_message = {0};
  can__rx_ExpectAndReturn(canbus, &rx_data, can_message.timeout, false);
  uint8_t message;
  can_bus_handle__receive_message(canbus, &can_message);
}