#include "unity.h"

#include <stdio.h>

#include "lab_queue_static.c"

#define SIZE_OF_QUEUE 200

static queue_s lab_queue;
static uint8_t queue_size[SIZE_OF_QUEUE];

void setUp() {}

void tearDown() {}

void test_queue_static__init(void) {
  queue_s temp = {0};
  uint8_t temp_queue_array[SIZE_OF_QUEUE] = {0};
  temp.queue_memory = temp_queue_array;
  temp.queue_size = SIZE_OF_QUEUE;
  queue_static__init(&lab_queue, queue_size, sizeof(queue_size));
  TEST_ASSERT_EQUAL_MEMORY((temp.queue_memory), (lab_queue.queue_memory), SIZE_OF_QUEUE);
}

void test_queue_static__push(void) { TEST_ASSERT_TRUE(queue_static__push(&lab_queue, 10)); }

void test_queue_static__pop(void) {
  uint8_t data = 0;
  ;
  TEST_ASSERT_TRUE(queue_static__pop(&lab_queue, &data));
}

void test_queue_static__push_pop(void) {
  uint8_t data = 0;
  TEST_ASSERT_TRUE_MESSAGE(queue_static__push(&lab_queue, 12), "test_queue__push_pop:Item not Pushed\n");
  TEST_ASSERT_TRUE_MESSAGE(queue_static__pop(&lab_queue, &data), "test_queue__push_pop:Item not poped\n");
  TEST_ASSERT_EQUAL_UINT8(12, data);
}

void test_queue_static__push_pop_2_elements(void) {
  uint8_t data[2] = {0};
  TEST_ASSERT_TRUE_MESSAGE(queue_static__push(&lab_queue, 10), "test_queue__push_pop:Item not Pushed\n");
  TEST_ASSERT_TRUE_MESSAGE(queue_static__push(&lab_queue, 11), "test_queue__push_pop:Item not Pushed\n");
  TEST_ASSERT_TRUE_MESSAGE(queue_static__pop(&lab_queue, &data[0]), "test_queue__push_pop:Item not poped\n");
  TEST_ASSERT_TRUE_MESSAGE(queue_static__pop(&lab_queue, &data[1]), "test_queue__push_pop:Item not poped\n");
  TEST_ASSERT_EQUAL_UINT8(10, data[0]);
  TEST_ASSERT_EQUAL_UINT8(11, data[1]);
}

void test_queue_static__push_pop_multiple_elements(void) {
  uint8_t data = 0;
  for (int i = 0; i < lab_queue.queue_size; i++) {
    TEST_ASSERT_TRUE_MESSAGE(queue_static__push(&lab_queue, i), "test_queue__push_pop:Item not Pushed\n");
  }
  for (int i = 0; i < (lab_queue.queue_size / 2); i++) {
    TEST_ASSERT_TRUE_MESSAGE(queue_static__pop(&lab_queue, &data), "test_queue__push_pop:Item not Pushed\n");
    TEST_ASSERT_EQUAL_UINT8(i, data);
  }
  for (int i = lab_queue.queue_size; i < (lab_queue.queue_size + lab_queue.queue_size / 2); i++) {
    TEST_ASSERT_TRUE_MESSAGE(queue_static__push(&lab_queue, i), "test_queue__push_pop:Item not Pushed\n");
  }
  for (int i = (lab_queue.queue_size / 2); i < (lab_queue.queue_size + lab_queue.queue_size / 2); i++) {
    TEST_ASSERT_TRUE_MESSAGE(queue_static__pop(&lab_queue, &data), "test_queue__push_pop:Item not Pushed\n");
    TEST_ASSERT_EQUAL_UINT8(i, data);
  }
}

void test_queue_static__get_item_count(void) {
  size_t queue_used_size = 0;
  queue_used_size = queue_static__get_item_count(&lab_queue);
  TEST_ASSERT_EQUAL_UINT8(lab_queue.queue_top, queue_used_size);
}

void test_queue_static__push_overflow(void) {
  size_t queue_used_size = queue_static__get_item_count(&lab_queue);
  for (uint8_t i = 0; i < (SIZE_OF_QUEUE - queue_used_size); i++) {
    TEST_ASSERT_TRUE(queue_static__push(&lab_queue, i));
  }
  TEST_ASSERT_FALSE(queue_static__push(&lab_queue, 10));
}

void test_queue_static__pop_overflow(void) {
  size_t queue_used_size = queue_static__get_item_count(&lab_queue);
  uint8_t data = 0;
  for (uint8_t i = 0; i < (queue_used_size); i++) {
    TEST_ASSERT_TRUE(queue_static__pop(&lab_queue, &data));
  }
  TEST_ASSERT_FALSE(queue_static__pop(&lab_queue, &data));
}

void test_queue_static__shift_elements(void) {
  queue_s test_queue;
  uint8_t test_array[10];
  test_queue.queue_memory = test_array;
  test_queue.queue_memory[0] = 1;
  test_queue.queue_memory[1] = 2;
  test_queue.queue_top = 2;
  test_queue.queue_size = 10;
  queue_static__shift_elements(&test_queue);
  TEST_ASSERT_EQUAL_UINT8(2, test_queue.queue_memory[0]);
  TEST_ASSERT_EQUAL_UINT8(1, test_queue.queue_top);
}
