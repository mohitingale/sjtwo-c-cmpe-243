#include "can_message_sensor_handle.h"
#include "gps.h"
#include <stdio.h>
#include <string.h>

bool can_sensor_send_gps_data(can__num_e canbus) {
  gps_coordinates_t gps_data;
  gps_data = gps__get_coordinates();
  can_message_handle_t can_gps_message = {0};
  can_gps_message.message_id = 2;
  memcpy(&can_gps_message.data, &gps_data, sizeof(gps_data));
  can_gps_message.timeout = 0;
  can_gps_message.data_length = 8;
  return can_bus_handle__send_message(canbus, can_gps_message);
}

bool can_sensor_receive_gps_data(can__num_e canbus, gps_coordinates_t *gps_data) {
  bool return_val = false;
  can_message_handle_t can_gps_message = {0};
  while (can_bus_handle__receive_message(canbus, &can_gps_message)) {
    memcpy(gps_data, &can_gps_message.data, sizeof(gps_coordinates_t));
    return_val = true;
  }
  return return_val;
}
