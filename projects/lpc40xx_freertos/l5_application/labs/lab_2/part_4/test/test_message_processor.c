#include "unity.h"

#include <string.h>

#include "Mockmessage.h"

#include "message_processor.c"

static bool message__read_dollar_in_message_hijacked_stub(message_s *message_to_read, int callback_count) {
  static char *message = "$hello";
  memcpy(message_to_read->data, message, sizeof(message_to_read->data));
  return true;
}

static bool message__read_without_dollar_in_message_hijacked_stub(message_s *message_to_read, int callback_count) {
  static char *message = "hello";
  memcpy(message_to_read->data, message, sizeof(message_to_read->data));
  return true;
}

// This only tests if we process at most 3 messages
void test_process_3_messages(void) {
  static message_s message1 = {"Test1"};
  static message_s message2 = {"Test2"};
  static message_s message3 = {"Test3"};

  message__read_ExpectAndReturn(NULL, true);
  message__read_IgnoreArg_message_to_read();
  message__read_ReturnThruPtr_message_to_read(&message1);

  message__read_ExpectAndReturn(NULL, true);
  message__read_IgnoreArg_message_to_read();
  message__read_ReturnThruPtr_message_to_read(&message2);

  message__read_ExpectAndReturn(NULL, true);
  message__read_IgnoreArg_message_to_read();
  message__read_ReturnThruPtr_message_to_read(&message3);

  // Since we did not return a message that starts with '$' this should return false
  TEST_ASSERT_FALSE(message_processor());
}

void test_process_message_with_dollar_sign(void) {
  message__read_StubWithCallback(message__read_dollar_in_message_hijacked_stub);
  TEST_ASSERT_TRUE(message_processor());
}

void test_process_messages_without_any_dollar_sign(void) {
  message__read_StubWithCallback(message__read_without_dollar_in_message_hijacked_stub);
  TEST_ASSERT_FALSE(message_processor());
}

void test_process_message_read_fail(void) {
  message__read_ExpectAndReturn(NULL, false);
  message__read_IgnoreArg_message_to_read();
  TEST_ASSERT_FALSE(message_processor());
}

void test_message_processor_check_dollar_in_message_present() {
  message_s temp = {"$test"};
  TEST_ASSERT_TRUE(message_processor_check_dollar_in_message(&temp));
}

void test_message_processor_check_dollar_in_message_not_present() {
  message_s temp = {"test"};
  TEST_ASSERT_FALSE(message_processor_check_dollar_in_message(&temp));
}