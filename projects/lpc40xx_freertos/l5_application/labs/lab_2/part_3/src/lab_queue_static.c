#include "lab_queue_static.h"

void queue_static__init(queue_s *queue, void *static_queue_memory, size_t size_of_static_memory) {
  queue->queue_memory = static_queue_memory;
  queue->queue_size = size_of_static_memory;
  for (uint8_t i = 0; i < queue->queue_size; i++) {
    queue->queue_memory[i] = 0;
  }
  queue->queue_top = 0;
}

static void queue_static__shift_elements(queue_s *queue) {
  for (uint8_t i = 0; i < (queue->queue_top - 1); i++) {
    queue->queue_memory[i] = queue->queue_memory[i + 1];
  }
  (queue->queue_top)--;
}

bool queue_static__push(queue_s *queue, uint8_t push_value) {
  if (queue_static__get_item_count(queue) < queue->queue_size) {
    queue->queue_memory[(queue->queue_top)++] = push_value;
    return true;
  } else {
    return false;
  }
}

bool queue_static__pop(queue_s *queue, uint8_t *pop_value) {
  if (queue_static__get_item_count(queue) > 0) {

    *pop_value = queue->queue_memory[0];
    queue->queue_memory[0] = 0;
    queue_static__shift_elements(queue);
    return true;
  } else {
    return false;
  }
}

size_t queue_static__get_item_count(const queue_s *queue) { return queue->queue_top; }