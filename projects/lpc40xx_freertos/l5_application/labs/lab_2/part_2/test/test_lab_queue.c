#include "unity.h"

#include <stdio.h>

#include "lab_queue.c"

static queue_s lab_queue;

void setUp() {}

void tearDown() {}

void test_queue__init(void) {
  queue_s temp = {0};
  memset(&temp, 0, sizeof(queue_s));
  queue__init(&lab_queue);
  TEST_ASSERT_EQUAL_MEMORY(&temp, &lab_queue, sizeof(queue_s));
}

void test_queue__push(void) { TEST_ASSERT_TRUE(queue__push(&lab_queue, 10)); }

void test_queue__pop(void) {
  uint8_t data = 0;
  ;
  TEST_ASSERT_TRUE(queue__pop(&lab_queue, &data));
}

void test_queue__push_pop(void) {
  uint8_t data = 0;
  TEST_ASSERT_TRUE_MESSAGE(queue__push(&lab_queue, 12), "test_queue__push_pop:Item not Pushed\n");
  TEST_ASSERT_TRUE_MESSAGE(queue__pop(&lab_queue, &data), "test_queue__push_pop:Item not poped\n");
  TEST_ASSERT_EQUAL_UINT8(12, data);
}

void test_queue__push_pop_2_elements(void) {
  uint8_t data[2] = {0};
  TEST_ASSERT_TRUE_MESSAGE(queue__push(&lab_queue, 10), "test_queue__push_pop:Item not Pushed\n");
  TEST_ASSERT_TRUE_MESSAGE(queue__push(&lab_queue, 11), "test_queue__push_pop:Item not Pushed\n");
  TEST_ASSERT_TRUE_MESSAGE(queue__pop(&lab_queue, &data[0]), "test_queue__push_pop:Item not poped\n");
  TEST_ASSERT_TRUE_MESSAGE(queue__pop(&lab_queue, &data[1]), "test_queue__push_pop:Item not poped\n");
  TEST_ASSERT_EQUAL_UINT8(10, data[0]);
  TEST_ASSERT_EQUAL_UINT8(11, data[1]);
}

void test_queue__push_pop_multiple_elements(void) {
  uint8_t data = 0;
  for (int i = 0; i < QUEUE_SIZE; i++) {
    TEST_ASSERT_TRUE_MESSAGE(queue__push(&lab_queue, i), "test_queue__push_pop:Item not Pushed\n");
  }
  for (int i = 0; i < (QUEUE_SIZE / 2); i++) {
    TEST_ASSERT_TRUE_MESSAGE(queue__pop(&lab_queue, &data), "test_queue__push_pop:Item not Pushed\n");
    TEST_ASSERT_EQUAL_UINT8(i, data);
  }
  for (int i = QUEUE_SIZE; i < (QUEUE_SIZE + QUEUE_SIZE / 2); i++) {
    TEST_ASSERT_TRUE_MESSAGE(queue__push(&lab_queue, i), "test_queue__push_pop:Item not Pushed\n");
  }
  for (int i = (QUEUE_SIZE / 2); i < (QUEUE_SIZE + QUEUE_SIZE / 2); i++) {
    TEST_ASSERT_TRUE_MESSAGE(queue__pop(&lab_queue, &data), "test_queue__push_pop:Item not Pushed\n");
    TEST_ASSERT_EQUAL_UINT8(i, data);
  }
}

void test_queue__get_item_count(void) {
  size_t queue_used_size = 0;
  queue_used_size = queue__get_item_count(&lab_queue);
  TEST_ASSERT_EQUAL_UINT8(lab_queue.queue_top, queue_used_size);
}

void test_queue__push_overflow(void) {
  size_t queue_used_size = queue__get_item_count(&lab_queue);
  for (uint8_t i = 0; i < (QUEUE_SIZE - queue_used_size); i++) {
    TEST_ASSERT_TRUE(queue__push(&lab_queue, i));
  }
  TEST_ASSERT_FALSE(queue__push(&lab_queue, 10));
}

void test_queue__pop_overflow(void) {
  size_t queue_used_size = queue__get_item_count(&lab_queue);
  uint8_t data = 0;
  for (uint8_t i = 0; i < (queue_used_size); i++) {
    TEST_ASSERT_TRUE(queue__pop(&lab_queue, &data));
  }
  TEST_ASSERT_FALSE(queue__pop(&lab_queue, &data));
}

void test_queue__shift_elements(void) {
  queue_s test_queue;
  test_queue.queue_memory[0] = 1;
  test_queue.queue_memory[1] = 2;
  test_queue.queue_top = 2;
  queue__shift_elements(&test_queue);
  TEST_ASSERT_EQUAL_UINT8(2, test_queue.queue_memory[0]);
  TEST_ASSERT_EQUAL_UINT8(1, test_queue.queue_top);
}
